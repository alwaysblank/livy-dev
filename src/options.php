<?php

use Carbon_Fields\Container;
use Carbon_Fields\Field;
use Coduo\PHPHumanizer\StringHumanizer;

if (current_user_can('livy_settings_access')) :

    $block_choices = array();
    foreach ($this->getPossibleBlockTypes() as $type) {
        $block_choices[$type] = StringHumanizer::humanize($type);
    }

    $posttype_choices = array();
    foreach ($this->getPossiblePostTypes() as $type => $name) {
        $posttype_choices[$type] = StringHumanizer::humanize($name);
    }

    $role_choices = array();
    foreach ($this->getPossibleAdminRoles() as $role) {
        $role_choices[$role] = StringHumanizer::humanize($role);
    }

    Container::make('theme_options', __('Livy', 'livy'))
        ->set_icon('dashicons-welcome-widgets-menus')
        ->add_tab(__('General'), array(
            Field::make('set', 'livy_block_types', __('Block Types', 'livy'))
                ->help_text(__('Checked block types will be displayed. By default, all types are shown, but you can uncheck some to prevent them from appearing as options.', 'livy'))
                ->set_width(50)
                ->add_options($block_choices),
            Field::make('set', 'livy_post_types', __('Post Types', 'livy'))
                ->set_width(50)
                ->help_text(__('The Composer will be used on the checked post types. By default, some sensible options are set, but you can modify them by checking or unchecking items.', 'livy'))
                ->add_options($posttype_choices),
            Field::make('set', 'livy_user_roles', __('Admin User Roles', 'livy'))
                ->help_text(__('Any checked roles will be able to access Composer settings. Administrator access cannot be disabled here.', 'livy'))
                ->add_options($role_choices),
        ));

endif;
