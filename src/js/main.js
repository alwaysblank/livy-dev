jQuery(document).ready(function() {

    (function($) {

        // Replace all select fields with the select2 improved select
        $('.container-Composer select').select2();

        // Limit the number of characters that can be entered into certain fields
        $('input[data-char-limit]').each(function() {
            var $self = $(this);
            var input = $self[0];
            Countable.live(input, function(counter) {
                if (counter.characters > parseInt($self.data('char-limit'))) {
                    $('#publishing-action input[type="submit"]').prop("disabled", true);
                    if (!$self.siblings('.too-many-characters').length) {
                        $self.after('<div class="__livy__admin too-many-characters"><span class="warning">This content is too long.</span><span class="count"></span></div>');
                    }
                    $self.siblings('.too-many-characters').find('.count').html(counter.characters + ' / <strong>' + $self.data('char-limit') + '</strong>');
                } else if (counter.characters < parseInt($self.data('char-limit'))) {
                    $('#publishing-action input[type="submit"]').prop("disabled", false);
                    $self.siblings('.too-many-characters').remove();
                }
            });
        });

        // Add a snippet of the content to the top bar of each block, to help identify them when collapsed
        function insertBlockPreviewText($self, $input) {
            if ($input.length > 0) {
                var text = $input.attr('value');
                var fill = "Cannot generate preview";
                if (text.length > 0) {
                    fill = text.substring(0, 200).trim();
                }
                $self.find('.__livy__admin__block-preview').text(fill);
            }
        }

        function addBlockPreview($el) {
            var $self = $el;
            var $input = $self.find('.preview-text-field .field-holder').find('input, textarea').first();
            if ($input.length < 1) {
                $input = $self.find('.field-holder').find('input, textarea').first();
            }
            console.log();
            if ($self.find('.__livy__admin__block-preview').length < 1) {
                $self.find('.carbon-drag-handle').append('<span class="__livy__admin__block-preview"></span>');
            }
            $input.change(function() {
                var text = $(this).attr('value');
                $self.find('.__livy__admin__block-preview').text(text.substring(0, 200).trim());
            });
            insertBlockPreviewText($self, $input);
        }

        // This adds it to all existing fields
        $('.carbon-group-row').each(function() {
            addBlockPreview($(this));
        });

        // This adds it to new fields
        $('.container-Composer').arrive('.carbon-group-row', function() {
            addBlockPreview($(this));
        });


    })(jQuery); // Fully reference jQuery after this point

});