<?php 
    use Carbon_Fields\Field;

    $blocks->add_fields($type, array(
            Field::make('rich_text', "{$type}_text", __('Text', 'livy'))
                ->add_class('__livy__admin__hide-label'),
        ));
