<?php 

namespace Carbon_Fields\Field;

class Tagline_Field extends Text_Field
{
    /**
         * Underscore template of this field.
         */
        public function template()
        {
            ?>
            <input id="{{{ id }}}" type="text" name="{{{ name }}}" value="{{ value }}" data-char-limit="150" class="tagline-text" />
            <?php

        }
}

     $blocks->add_fields($type, array(
            Field::make('tagline', "{$type}_text", __('Tagling', 'livy'))
                ->add_class('__livy__admin__hide-label'),
        ));
