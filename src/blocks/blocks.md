# Building Blocks

### File Structure

Block definitions and layouts use a particular file structure, to keep everything straight. Every block must have at least two files:

- `def-[block_name].php`
- `layout-[block_name].php`

These files must be in a folder named `_[block_name]` in the `src/blocks` folder.

In other words, if you're creating a block named `exam`, the layout should look like this:

- `src/`
    - `blocks/`
        - `_exam/`
            - `def-exam.php`
            - `layout-exam.php`

You can have other files in the block folder if you like--just make sure you have `def-` and `layout-`.

### Definition

The `def-[block_name]` file contains the code necessary to define the field using Carbon Forms and have it appear in the backend. It should follow this format:

```php
<?php
    use Carbon_Fields\Container;
    use Carbon_Fields\Field;

    $blocks->add_fields($type, array(
        Field::make("image", "{$type}_picture", "Photo")
    ));
```

See [Carbon Fields docs](https://carbonfields.net/docs/) for more information on how to define blocks.

The values we most need to concern ourselves with here, though, are `picture` and `crb_picture`. The first can be considered the `block_name` for this block, and wil be used to name the `def-` and `layout-` files. The second is the name of the field that we will use when getting data in the layout file.

This will make the block appear in the backend, when editing a post object. In order to have it actually display, however, we need to crete a layout file.

### Layout

The `layout-[block_name].php` file defines the layout of the block: How it will appear once rendered. Layouts generally use [Cyrus](https://bitbucket.org/benspants/cyrus) for templating, but technically they can use anything. Echo the data however you like: It is captured using php buffering and then returned later. The basic format for a layout file should looke like this:

```php
<?php 
    $picture = function ( $key, $val ) {
        $return = new Livy\Cyrus;
        $type =  ltrim( $val['_type'], "_" );
        $return->setEl('div')->setClass('__livy__picture')->addContent( wp_get_attachment_image( $val["{$type}_picture"], 'large' ) )->setClass('__livy__block')->display();
    };
    $picture( $key, $val );
    unset($picture);
```

Our action is wrapped in a closure to help avoid possible colisions. Closure variables should be named with the `block_name`, and should always be unset at the bottom of the file.

### Block List

Once you've defined your block and created a layout, add the `block_name` for your block to the `blockTypes` property of the Livy class.

**This is a bad practice, and should be revised when I have a better idea**
