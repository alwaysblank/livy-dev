<?php 
    $intro = function ($key, $val) {
        $return = new Livy\Cyrus();
        $type = ltrim($val['_type'], '_');
        $return->setEl('div')->setClass('__livy__intro')->addContent($val["{$type}_text"])->setClass('__livy__block')->display();
    };
    $intro($key, $val);
    unset($intro);
