<?php 
    use Carbon_Fields\Field;

    $blocks->add_fields($type, array(
            Field::make('textarea', "{$type}_text", __('Intro', 'livy'))
                ->add_class('__livy__admin__hide-label')
                    ->set_rows(4),
        ));
