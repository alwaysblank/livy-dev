<?php 
    $title = function ($key, $val) {
        $return = new Livy\Cyrus();
        $type = ltrim($val['_type'], '_');
        $return->setEl('h4')->setClass('__livy__title')->addContent($val["{$type}_text"])->setClass('__livy__block')->display();
    };
    $title($key, $val);
    unset($title);
