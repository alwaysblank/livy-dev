<?php

    use Carbon_Fields\Container;
    use Carbon_Fields\Field;

    /**
      * "Composer" here is not wrapped in a translate function because it is used by CF to 
      * generate an ID for the Composer block in the admin. Therefore changing this 
      * could theoretically break CSS/JS stuff. 
      */ 
    $composer = Container::make('post_meta', 'Composer');
    $composer->show_on_post_type($this->getActivePostTypes());

    $block_labels = array(
        'plural_name' => __('Blocks', 'livy'),
        'singular_name' => __('Block', 'livy'),
    );

    $blocks = Field::make('complex', 'livy_blocks', __('Blocks', 'livy'));
        foreach ($this->getActiveBlockTypes() as $type) :
            if ($override = locate_template("livy-def-$type.php")) :
                include $override;
                unset($override);
             else :
                $template_path = "_$type/def-$type.php";
                require_once $template_path;
                unset($template_path);
             endif;
        endforeach;
    $blocks->setup_labels($block_labels);

    $composer->add_fields(array($blocks));
